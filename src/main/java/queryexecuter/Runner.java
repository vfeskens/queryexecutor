package queryexecuter;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;

public class Runner {

    public static void main(String[] args) {

        if(args.length < 2) {
            throw new IllegalArgumentException("Expecting at least two arguments (1. jdbc url 2. query)");
        }

        final String jdbcUrl = args[0];
        final String query = args[1];
        final QueryType queryType = args.length == 3 ? QueryType.valueOf(args[2]) : QueryType.COUNT;

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        dataSource.setUrl(jdbcUrl);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        final int result;

        switch (queryType) {
            case COUNT: result = jdbcTemplate.queryForObject(query, Integer.class); break;
            case DELETE: result = jdbcTemplate.update(query); break;
            default: result = -1; break;
        }

        System.out.println("-------------------------------------------------------------");
        System.out.println("Jdbc url : " + jdbcUrl);
        System.out.println("Query : " + query);
        System.out.println("Query type : " + queryType);
        System.out.println("Result : " + result);
        System.out.println("-------------------------------------------------------------");
    }
}


