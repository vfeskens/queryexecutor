# Query Executor

Simple utility jar to perform database (count/delete) queries.

**Example usage**

1. mvn package

2. cd target/

3. Option 1 for **Count** query : java -jar queryexecuter-1.0.jar "jdbc:oracle:thin:system/oracle@//localhost:49161/xe" "select count(*) from system.newtable"

4. Option 2 for **Count** query : java -jar queryexecuter-1.0.jar "jdbc:oracle:thin:system/oracle@//localhost:49161/xe" "select count(*) from system.newtable" COUNT

5. for **Delete** query : java -jar queryexecuter-1.0.jar "jdbc:oracle:thin:system/oracle@//localhost:49161/xe" "delete from system.newtable" DELETE